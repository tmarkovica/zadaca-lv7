﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_krizic_kruzic
{
    interface ITurnBasedGame
    {
        bool Potez { get; set; }
        int Rez1 { get; set; }
        int Rez2 { get; set; }
        int BrojPartija { get; set; }
    }
}
