﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_krizic_kruzic
{
    class KrizicKruzic : ITurnBasedGame
    {
        private string imeIgraca1; // O
        private string imeIgraca2; // X
        private int rezultatIgraca1 = 0;
        private int rezultatIgraca2 = 0;
        private int ukupnoPartija = 0;
        private bool potez = false;

        public KrizicKruzic(string IME1, string IME2)
        {
            imeIgraca1 = IME1;
            imeIgraca2 = IME2;
        }

        public int Rez1
        {
            get { return rezultatIgraca1; }
            set { rezultatIgraca1 += 1; }
        }

        public int Rez2
        {
            get { return rezultatIgraca2; }
            set { rezultatIgraca2 += 1; }
        }

        public int BrojPartija
        {
            get { return ukupnoPartija; }
            set { ukupnoPartija += 1; }
        }

        public void PobjedaIgraca(char znak)
        {
            if (znak == 'O') Rez1++;
            else Rez2++;
        }

        public bool Potez
        {
            get { return potez; }
            set { potez = !potez; }
        }
    }
}
