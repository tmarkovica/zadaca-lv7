﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV7_krizic_kruzic
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            label2.Text = "O";
        }

        KrizicKruzic Igra;

        private void PostaviIgru()
        {
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";

            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
        }

        private void igraj(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (Igra.Potez) b.Text = "X";
            else b.Text = "O";

            b.Enabled = false;

            MijenjajPotez();

            ProvjeraPobjede();
        }
        
        private void MijenjajPotez()
        {
            Igra.Potez = true;

            if (Igra.Potez) label2.Text = "X";
            else label2.Text = "O";
        }

        private int br = 0;

        private void ProvjeraPobjede()
        {
            char[] znak = { 'X', 'O' };
            bool win = false;
            for(int i=0; i<2; i++)
            {
                if (button1.Text == znak[i].ToString() && button2.Text == znak[i].ToString() && button3.Text == znak[i].ToString()) win = true;
                else if (button1.Text == znak[i].ToString() && button4.Text == znak[i].ToString() && button7.Text == znak[i].ToString()) win = true;
                else if (button1.Text == znak[i].ToString() && button5.Text == znak[i].ToString() && button9.Text == znak[i].ToString()) win = true;
                else if (button4.Text == znak[i].ToString() && button5.Text == znak[i].ToString() && button6.Text == znak[i].ToString()) win = true;
                else if (button7.Text == znak[i].ToString() && button8.Text == znak[i].ToString() && button9.Text == znak[i].ToString()) win = true;
                else if (button2.Text == znak[i].ToString() && button5.Text == znak[i].ToString() && button8.Text == znak[i].ToString()) win = true;
                else if (button3.Text == znak[i].ToString() && button6.Text == znak[i].ToString() && button9.Text == znak[i].ToString()) win = true;
                else if (button3.Text == znak[i].ToString() && button5.Text == znak[i].ToString() && button7.Text == znak[i].ToString()) win = true;
            }

            if (win)
            {
                if (Igra.Potez)
                {
                    MessageBox.Show("Pobjednik: O");
                    Igra.PobjedaIgraca('O');
                }
                else
                {
                    MessageBox.Show("Pobjednik: X");
                    Igra.PobjedaIgraca('X');
                }
                br = 0;
                PostaviIgru();

                Igra.BrojPartija++;
                OsvjeziRezultat();
            }
            else
            {
                br++;
                if (br == 9) 
                {
                    MessageBox.Show("Nerjeseno");
                    PostaviIgru();
                    Igra.BrojPartija++;
                    OsvjeziRezultat();
                    br = 0;
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.IsNormalized() && textBox1.Text.IsNormalized() && textBox1.Text != textBox2.Text
                && textBox1.Text != "" && textBox2.Text != "")
            {
                Igra = new KrizicKruzic(textBox1.Text, textBox2.Text);

                OsvjeziRezultat();

                textBox1.Enabled = false;
                textBox2.Enabled = false;
                PostaviIgru();

                button10.Enabled = false;
            }
        }

        public void OsvjeziRezultat()
        {
            rez1.Text = Igra.Rez1.ToString();
            rez2.Text = Igra.Rez2.ToString();
            brojacPartija.Text = Igra.BrojPartija.ToString();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
